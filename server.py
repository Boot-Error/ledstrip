from bottle import route, run, template, request, response
import json
import serial_command

ser = serial_command.start()

values = {
   'r': 255,
   'g': 255,
   'b': 255
}

@route('/setcolor')
def set_color():

    rVal = int(request.query['r'], 10)
    gVal = int(request.query['g'], 10)
    bVal = int(request.query['b'], 10)

    values['r'] = rVal
    values['g'] = gVal
    values['b'] = bVal

    serial_command.sendColor(ser, rVal, gVal, bVal)

    return 'done'

@route('/getcolor')
def get_color():

    response.set_header('Content-Type', 'application/json')
    return json.dumps(values)


run(host='0.0.0.0', port=9080)
