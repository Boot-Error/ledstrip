#!/usr/bin/python3

import serial
from time import sleep
import random

def start():

    ser = serial.Serial('/dev/ttyACM0')
    ser.write(b'\n')

    return ser

def colorToHex(r, g, b):
    return bytes([r, b, g]) + b'\n'

def sendColor(ser, r, g, b):
    ser.write(colorToHex(r, g, b))

def end(ser):
    ser.close()

if __name__ == "__main__":

    ser = start()
    
    if len(sys.argv) > 3:

        r = int(sys.argv[1], 10)
        g = int(sys.argv[2], 10)
        b = int(sys.argv[3], 10)

        senColor(ser, r, g, b)
        end(ser)

    else:

        print("Not enough arguments supplied")



