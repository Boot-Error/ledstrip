
#define RED 11
#define GREEN 10
#define BLUE 9

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void setColor(int r, int g, int b) {
  analogWrite(RED, 255 - r);
  analogWrite(GREEN, 255 - g);
  analogWrite(BLUE, 255 - b);  
}

void diwali() {
  setColor(0, 0, 0);
  delay(1000);
  setColor(255, 0, 0);
  delay(1000);
  setColor(0, 255, 0);
  delay(1000);
  setColor(0, 0, 255);
  delay(1000);  
}

boolean complete = false;
void loop() {
  if (complete) {
    Serial.print("Done");
    complete = false;  
  }
}

int color[3];
int counter = 0;
void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar == '\n') {
       counter = 0;
       setColor(color[0], color[1], color[2]);
       complete = true;
       continue;
    }
    Serial.println((int)inChar);
    color[counter++] = (int)inChar;
  }  
}
