from tkinter import *
from tkinter.colorchooser import *
import requests

def send_color(r, g, b):
    requests.get('http://192.168.0.15/ledapi/setcolor?r={}&g={}&b={}'.format(int(r), int(g), int(b)))

def getColor():
    color = askcolor()[0]
    send_color(*color)

Button(text='Select Color', command=getColor).pack()
mainloop()
